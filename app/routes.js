// Dependencies
var JugadorCtrl = require('./controllers/jugadores.js');
var PartidoCtrl = require('./controllers/partidos.js');
var RankCtrl = require('./controllers/ranks.js');

// Opens App Routes
module.exports = function(express,app) {

// HOME
app.get('/', function(req, res, next) {
  res.sendfile('./public/index.html');
});

// JUGADOR
app.get('/jugador', function(req, res, next) {
  res.sendfile('./public/jugador.html');
});

// PARTIDO
app.get('/partido', function(req, res, next) {
  res.sendfile('./public/partido.html');
});

//API
var api = express.Router();
	//Jugadores
	api.route('/jugadores')  
	  .get(JugadorCtrl.findAll)
	  .post(JugadorCtrl.add);
	api.route('/jugadores/:id')  
	  .get(JugadorCtrl.findById)
	  .put(JugadorCtrl.update)
	  .delete(JugadorCtrl.delete);
  //Partidos
  api.route('/partidos')  
    .get(PartidoCtrl.findAll)
    .post(PartidoCtrl.add);
  api.route('/partidos/:id')  
    .get(PartidoCtrl.findById)
    .put(PartidoCtrl.update)
    .delete(PartidoCtrl.delete);
  //Ranking
  api.route('/ranks')  
    .get(RankCtrl.findAll)
app.use('/api/', api);  

};  