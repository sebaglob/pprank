var mongoose = require('mongoose');
var Jugador  = require('../models/jugador.js');
var Schema   = mongoose.Schema;

var partidoSchema = new Schema({
  gametype:       {type: Boolean, required: false },
  jugador11:      {type: String, required: true }, // Como hago para que sea "type: Jugador"?
  jugador12:      {type: String, required: false },
  jugador21:      {type: String, required: true },
  jugador22:      {type: String, required: false },
  setsGanador:    {type: Number, required: true },
  setsPerdedor:   {type: Number, required: true },
  puntosGanador:  {type: Array, required: false },
  puntosPerdedor: {type: Array, required: false },
  created_at:     {type: Date, default: Date.now},
  confirmed_at:   {type: Date, default: Date.now},
  updated_at:     {type: Date, default: Date.now}
});

// Si no se cargaron los puntos por set, el default es 21-19
partidoSchema.pre('save', function(next){
  // TODO: Recorrer los arrays de puntos y setearlos como corresponde segun el resultado de sets
  // this.puntosGanador = 21;
  // this.puntosPerdedor = 19;
  next();
});

// Sets the created_at parameter equal to the current time
partidoSchema.pre('save', function(next){
    now = new Date();
    this.updated_at = now;
    if(!this.created_at) {
        this.created_at = now
    }
    next();
});

module.exports = mongoose.model('Partido', partidoSchema);  