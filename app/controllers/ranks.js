// Dependencies
var mongoose        = require('mongoose');
var Jugador            = require('../models/jugador.js');

//GET - Return all registers
exports.findAll = function(req, res) {
  Jugador.find(function(err, jugadores) {
      if(err) return res.status(500).send(err.message);
    res.status(200).json(jugadores);
  });
};

//GET - Return a register with specified ID
exports.findById = function(req, res) {
  Jugador.findById(req.params.id, function(err, jugador) {
      if(err) return res.status(500).send(err.message);
    res.status(200).json(jugador);
  });
};

//POST - Insert a new register
exports.add = function(req, res) {
  console.log('Crear jugador');
  var jugador = new Jugador({
    name: req.body.name,
    surname: req.body.name,
    email: req.body.name
  });
  console.log('Salvar');
  jugador.save(function(err, jugador) {
    console.log('If');
    if(err) {
      console.log('Dio un error: ' + err)
    }
    if(err) return res.status(500).send(err.message);
    console.log('Entro al if');
    res.status(200).json(jugador);
  });
};

//PUT - Update a register already exists
exports.update = function(req, res) {
  Jugador.findById(req.params.id, function(err, jugador) {
    var jugador = new Jugador({
        name: req.body.name
    });
    jugador.save(function(err) {
      if(err) return res.status(500).send(err.message);
          res.status(200).json(jugador);
    });
  });
};

//DELETE - Delete a register with specified ID
exports.delete = function(req, res) {
  Jugador.findById(req.params.id, function(err, jugador) {
    jugador.remove(function(err) {
      if(err) return res.status(500).send(err.message);
          res.status(200).send();
    });
  });
};