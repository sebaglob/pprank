// Dependencies
var mongoose        = require('mongoose');
var Partido            = require('../models/partido.js');

//GET - Return all registers
exports.findAll = function(req, res) {
	Partido.find(function(err, partidos) {
    if(err) return res.status(500).send(err.message);
    console.log('Encontrados ' + partidos.lastIndexOf)
		res.status(200).json(partidos);
	});
};

//GET - Return a register with specified ID
exports.findById = function(req, res) {
	Partido.findById(req.params.id, function(err, partido) {
    	if(err) return res.status(500).send(err.message);
		res.status(200).json(partido);
	});
};

//POST - Insert a new register
exports.add = function(req, res) {
	console.log('Crear partido');
	var partido = new Partido({
    gametype: req.body.gametype,  // Singles: 0; Doubles: 1
    jugador11: req.body.jugador11,
    jugador21: req.body.jugador21,
    jugador12: req.body.jugador12,
    jugador22: req.body.jugador22,
    setsGanador: req.body.setsGanador,
    setsPerdedor: req.body.setsPerdedor,
    puntosGanador: req.body.puntosGanador,
    puntosPerdedor: req.body.puntosPerdedor
  });
  if(partido.gametype==undefined) {
    partido.gametype = false;
  };
	console.log('Salvar');
	partido.save(function(err, partido) {
		console.log('If');
		if(err) {
			console.log('Dio un error: ' + err)
		}
		if(err) return res.status(500).send(err.message);
		console.log('Entro al if');
		res.status(200).json(partido);
	});
};

//PUT - Update a register already exists
exports.update = function(req, res) {
	Partido.findById(req.params.id, function(err, partido) {
		var partido = new Partido({
	    	name: req.body.name
		});
		partido.save(function(err) {
			if(err) return res.status(500).send(err.message);
      		res.status(200).json(partido);
		});
	});
};

//DELETE - Delete a register with specified ID
exports.delete = function(req, res) {
	Partido.findById(req.params.id, function(err, partido) {
		partido.remove(function(err) {
			if(err) return res.status(500).send(err.message);
      		res.status(200).send();
		});
	});
};