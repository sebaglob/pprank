angular.module('angularPP', []);  
function mainController($scope, $http) {  
  $scope.newJugador = {};
  $scope.newPartido = {};
  getRanks();
  getGames();

  function getRanks(){
    $http.get('/api/jugadores')
          .success(function(data) {
            $scope.ranks = data;
          })
          .error(function(data) {
            console.log('Error: ' + data);
          });
  };

  // Cuando se añade un nuevo Jugador, manda los datos a la API
  $scope.createJugador = function(){
    $http.post('/api/jugadores', $scope.newJugador)
          .success(function(data) {
            $scope.newJugador = {};
            $scope.jugador = data;
          })
          .error(function(data) {
            console.log('Error: ' + data);
          });
    // Una vez agregado el jugador, recargo la lista
    getRanks();
  };
 
  function getGames(){
    $http.get('/api/partidos')
          .success(function(data) {
            $scope.games = data;
          })
          .error(function(data) {
            console.log('Error: ' + data);
          });
  }; 
  // Cuando se añade un nuevo Partido, manda los datos a la API
  $scope.createPartido = function(){
    $http.post('/api/partidos', $scope.newPartido)
          .success(function(data) {
            $scope.newPartido = {};
            $scope.partido = data;
          })
          .error(function(data) {
            console.log('Error: ' + data);
          });
    // Una vez agregado el partido, recargo la lista
    getGames();
  };
}